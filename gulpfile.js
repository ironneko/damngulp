var gulp = require('gulp');
var $ = require('gulp-load-plugins')(), // 只加入gulp開頭的套件名稱，不需要額外request
    mainBowerFiles = require('main-bower-files'),
    browserSync = require('browser-sync'), // Web Server，包含Livereload
    autoprefixer = require('autoprefixer'); // 自動為你的 CSS 補上前綴詞
    nunjucksRender = require('gulp-nunjucks-render'); //html+nunjucks模版
var minimist = require('minimist'); // 用來讀取指令轉成變數
var gulpSequence = require('gulp-sequence').use(gulp);

// ====== 輸出指令開始 ======
// gulp                          一般切版用
// gulp dev                      VS專案用
// gulp build --env production   結案時清理環境壓縮用
// ====== 輸出指令結束 ======

// production || development
// 佈署環境切換
var envOptions = {
  string: 'env',
  default: {
    env: 'development'
  }
};
var options = minimist(process.argv.slice(2), envOptions);
console.log(options);

// 輸出入變數路徑
var path = {
  source: 'src/', // 原始碼
  public: 'dist/' // 輸出位置
}

// 啟動前清除環境
gulp.task('clean', function() {
  return gulp.src([path.public, './.tmp'], {
      allowEmpty: true,
      read: false
    }) // 選項讀取：false阻止gulp讀取文件的內容，使此任務更快。
    .pipe($.clean());
  cb(err)
});


// js轉換編譯
gulp.task('babel', function() {
  return gulp.src(['src/js/**/*.js'])
    .pipe($.plumber())
    .pipe($.sourcemaps.init()) // 可以找到寫在哪個檔案裡
    .pipe(
      $.if(options.env === 'production', $.uglify({
        compress: {
          drop_console: true
        }
      }))
    )
    .pipe($.if(options.env === 'production', $.concat('all.js')))  // 假設結案則壓縮 JS成一隻
    .pipe($.sourcemaps.write('../maps'))
    .pipe(gulp.dest(path.public + 'js'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

// html layout
gulp.task('layout', function() {
  return gulp.src(path.source + '/*.+(html|nunjucks)')
    .pipe(nunjucksRender({
      path: [path.source + 'templates']
    }))
    .pipe(gulp.dest(path.public))
    .pipe(browserSync.reload({
      stream: true
    }));
});

// 第三方套件下載   github.com  bower.json 載入控制版本
gulp.task('bower', function() {
  return gulp.src(mainBowerFiles())
    .pipe(gulp.dest('./.tmp/vendors'));
  cb(err);
});

// 跑完bower task才執行vendorJs
gulp.task('vendorJs', gulp.series('bower', function(done) {
  return gulp.src(['./.tmp/vendors/**/**.js'])
    .pipe($.order([
      'jquery.js'
    ]))
    // .pipe($.concat('vendor.js')) // 把載下來的外部js合併成一個
    .pipe($.if(options.env === 'production', $.uglify()))
    .pipe(gulp.dest(path.public + 'js'));
    done();
}))

// scss 編譯
gulp.task('sass', function() {
  // PostCSS AutoPrefixer
  var processors = [
    autoprefixer({
      browsers: ['last 5 version'], //看要符合幾個版本 5/4/3/2/1
    })
  ];

  return gulp.src(path.source + 'scss/**/*.scss')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass({
        outputStyle: 'nested'    //==== expanded/nested/compact/compressed
      })
      .on('error', $.sass.logError))
    .pipe($.postcss(processors))
    .pipe($.if(options.env === 'production', $.cleanCss())) // 假設開發環境則壓縮 CSS
    .pipe($.sourcemaps.write('../maps'))  //用來看scss 路徑用
    .pipe(gulp.dest(path.public + 'css'))
    .pipe(browserSync.reload({
      stream: true
    }));;
});

// 圖檔壓縮
// 一般採複製模式,收專案時才統一壓縮
gulp.task('imageMin', function(done) {
  gulp.src(path.source + 'images/**/*')
    .pipe($.if(options.env === 'production', $.imagemin()))
    .pipe(gulp.dest(path.public + 'images'));
    done();
});

// 複製 Fonts 資料匣    複製到src & dist
gulp.task('icons', function() {
  return gulp.src([
    './bower_components/font-awesome/fonts/*.*'
  ])
    .pipe(gulp.dest(path.source + 'fonts/'))
    .pipe(gulp.dest(path.public + 'fonts/'));
});

// 瀏覽器同步檢視
gulp.task('browserSync', function() {
  browserSync.init({
    port:9000,
    livereload:true,
    server: {
      baseDir: path.public
    }
  })
});

// 監聽任務
gulp.task('watch', function(done) {

  // 監視 SCSS 變更
  gulp.watch([path.source + 'scss/**/*.scss'], gulp.series('sass'));

  // 監視 HTML 變更
  gulp.watch([path.source + '**/*.html'], gulp.series('layout'));

  // 監視 JS 變更
  gulp.watch([path.source + 'js/**/*.js'], gulp.series('babel'));

  // 監視圖片新增就複製  結案才執行壓縮
  gulp.watch([path.source + 'images/**/*'], gulp.series('imageMin'));

  done();
});

// 一般開發輸出
//gulp.series(照順序執行)
//gulp.parallel(同時執行)

gulp.task(
  'default',
  gulp.series ('clean', 'layout', 'sass', 'vendorJs','babel', 'imageMin', 'icons', 'watch', 'browserSync')
);

// 壓縮結案時
gulp.task(
  'build',
  gulp.series ('clean','layout', 'sass', 'vendorJs','babel', 'imageMin', 'icons')
);

// 進專案只保留SCSS編譯用
gulp.task(
  'dev',
  gulp.series ('clean','sass', 'watch')
);
