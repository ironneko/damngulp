# README #

gulp 執行說明檔案

### 如何變更輸出輸入路徑 ###

var path = {
  source: 'src/',  // 原始碼
  public: 'dist/'  // 輸出位置
}

### 不同的啟動方式 ###

* VS專案用      #gulp dev
* 一般切版用     #gulp
* 收拾專案       #gulp build --env production

### 這版本可用什麼功能 ###

* Susy/breakpoint/bootstrap 掛載
* compass-sass 掛載

### 第三方套件使用 ###

* jquery ^3.3.1
* font-awesome ^5.2.0
<!-- * simple-line-icons ^2.4.1 -->
* susy ^3.0.5


### 需要複製誰 ###

* gulpfile.js    執行任務核心
* package.json   需要安裝gulp 套件的列表
* bower.json     第三方套件的安裝列表

### 執行順序 ###

1.複製上方檔案
2.npm install
3.bower install
4.gulp or gulp build

祝順利啟動!!
